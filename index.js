//Bài 1
document.getElementById("btnHomQua").onclick = function () {
  var ngay = document.getElementById("ngay").value * 1;
  var thang = document.getElementById("thang").value * 1;
  var nam = document.getElementById("nam").value * 1;

  var today = new Date(`${thang},${ngay},${nam}`);

  var yesterday = new Date(today);
  yesterday.setDate(yesterday.getDate() - 1);

  var yesterdayDay = yesterday.getDate();
  var yesterdayMonth = yesterday.getMonth() + 1;
  var yesterdayYear = yesterday.getFullYear();

  document.getElementById(
    "ketQuaBai1"
  ).innerHTML = `${yesterdayDay}/${yesterdayMonth}/${yesterdayYear}`;
};

document.getElementById("btnNgayMai").onclick = function () {
  var ngay = document.getElementById("ngay").value * 1;
  var thang = document.getElementById("thang").value * 1;
  var nam = document.getElementById("nam").value * 1;

  var today = new Date(`${thang},${ngay},${nam}`);

  var tomorrow = new Date(today);
  tomorrow.setDate(tomorrow.getDate() + 1);

  var tomorrowDay = tomorrow.getDate();
  var tomorrowMonth = tomorrow.getMonth() + 1;
  var tomorrowYear = tomorrow.getFullYear();

  document.getElementById(
    "ketQuaBai1"
  ).innerHTML = `${tomorrowDay}/${tomorrowMonth}/${tomorrowYear}`;
};

// Bài 2
document.getElementById("btnTinhNgay").onclick = function () {
  var thang = document.getElementById("thangBai2").value * 1;
  var nam = document.getElementById("namBai2").value * 1;

  var ngay = 0;
  if (
    thang == 1 ||
    thang == 3 ||
    thang == 5 ||
    thang == 7 ||
    thang == 8 ||
    thang == 10 ||
    thang == 12
  ) {
    ngay = 31;
  } else if (thang == 4 || thang == 6 || thang == 9 || thang == 11) {
    ngay = 30;
  } else if (thang == 2) {
    if ((nam % 4 == 0 && nam % 100 != 0) || nam % 400 == 0) {
      ngay = 29;
    } else {
      ngay = 28;
    }
  }

  document.getElementById(
    "ketQuaBai2"
  ).innerHTML = `Tháng ${thang} năm ${nam} có ${ngay} ngày`;
};

// Bài 3

document.getElementById("btnDocSo").onclick = function () {
  var soCanDoc = document.getElementById("soCanDoc").value * 1;

  var hangTram = Math.floor(soCanDoc / 100);
  var hangChuc = Math.floor((soCanDoc % 100) / 10);
  var hangDonVi = (soCanDoc % 100) % 10;

  var docHangTram = "";
  var docHangChuc = "";
  var docHangDonVi = "";

  if (hangTram == 0 && hangChuc == 0 && hangDonVi == 0) {
    alert("Số hàng trăm không được xác định");
    alert("Số hàng chục không được xác định");
    alert("Số hàng đơn vị không được xác định");
  } else if (hangTram == 0 && hangChuc == 0) {
    alert("Số hàng trăm không được xác định");
    alert("Số hàng chục không được xác định");
    switch (hangDonVi) {
      case 0:
        docHangDonVi = "Không";
        break;
      case 1:
        docHangDonVi = "Một";
        break;
      case 2:
        docHangDonVi = "Hai";
        break;
      case 3:
        docHangDonVi = "Ba";
        break;
      case 4:
        docHangDonVi = "Bốn";
        break;
      case 5:
        docHangDonVi = "Năm";
        break;
      case 6:
        docHangDonVi = "Sáu";
        break;
      case 7:
        docHangDonVi = "Bảy";
        break;
      case 8:
        docHangDonVi = "Tám";
        break;
      case 9:
        docHangDonVi = "Chín";
        break;
    }
    document.getElementById("ketQuaBai3").innerHTML = `${docHangDonVi}`;
  } else if (hangTram == 0 && hangDonVi == 0) {
    alert("Số hàng trăm không được xác định");
    alert("Số hàng đơn vị không được xác định");
    switch (hangChuc) {
      case 0:
        docHangChuc = "Không";
        break;
      case 1:
        docHangChuc = "Một";
        break;
      case 2:
        docHangChuc = "Hai";
        break;
      case 3:
        docHangChuc = "Ba";
        break;
      case 4:
        docHangChuc = "Bốn";
        break;
      case 5:
        docHangChuc = "Năm";
        break;
      case 6:
        docHangChuc = "Sáu";
        break;
      case 7:
        docHangChuc = "Bảy";
        break;
      case 8:
        docHangChuc = "Tám";
        break;
      case 9:
        docHangChuc = "Chín";
        break;
    }
    document.getElementById("ketQuaBai3").innerHTML = `${docHangChuc} muơi`;
  } else if (hangChuc == 0 && hangDonVi == 0) {
    alert("Số hàng chục không được xác định");
    alert("Số hàng đơn vị không được xác định");
    switch (hangTram) {
      case 0:
        docHangTram = "Không";
        break;
      case 1:
        docHangTram = "Một";
        break;
      case 2:
        docHangTram = "Hai";
        break;
      case 3:
        docHangTram = "Ba";
        break;
      case 4:
        docHangTram = "Bốn";
        break;
      case 5:
        docHangTram = "Năm";
        break;
      case 6:
        docHangTram = "Sáu";
        break;
      case 7:
        docHangTram = "Bảy";
        break;
      case 8:
        docHangTram = "Tám";
        break;
      case 9:
        docHangTram = "Chín";
        break;
    }
    document.getElementById("ketQuaBai3").innerHTML = `${docHangTram} trăm`;
  } else if (hangTram == 0) {
    alert("Số hàng trăm không được xác định");
    switch (hangChuc) {
      case 0:
        docHangChuc = "Không";
        break;
      case 1:
        docHangChuc = "Một";
        break;
      case 2:
        docHangChuc = "Hai";
        break;
      case 3:
        docHangChuc = "Ba";
        break;
      case 4:
        docHangChuc = "Bốn";
        break;
      case 5:
        docHangChuc = "Năm";
        break;
      case 6:
        docHangChuc = "Sáu";
        break;
      case 7:
        docHangChuc = "Bảy";
        break;
      case 8:
        docHangChuc = "Tám";
        break;
      case 9:
        docHangChuc = "Chín";
        break;
    }
    switch (hangDonVi) {
      case 0:
        docHangDonVi = "Không";
        break;
      case 1:
        docHangDonVi = "Một";
        break;
      case 2:
        docHangDonVi = "Hai";
        break;
      case 3:
        docHangDonVi = "Ba";
        break;
      case 4:
        docHangDonVi = "Bốn";
        break;
      case 5:
        docHangDonVi = "Năm";
        break;
      case 6:
        docHangDonVi = "Sáu";
        break;
      case 7:
        docHangDonVi = "Bảy";
        break;
      case 8:
        docHangDonVi = "Tám";
        break;
      case 9:
        docHangDonVi = "Chín";
        break;
    }
    document.getElementById(
      "ketQuaBai3"
    ).innerHTML = `${docHangChuc} mươi ${docHangDonVi}`;
  } else if (hangChuc == 0) {
    alert("Số hàng chục không được xác định");
    switch (hangTram) {
      case 0:
        docHangTram = "Không";
        break;
      case 1:
        docHangTram = "Một";
        break;
      case 2:
        docHangTram = "Hai";
        break;
      case 3:
        docHangTram = "Ba";
        break;
      case 4:
        docHangTram = "Bốn";
        break;
      case 5:
        docHangTram = "Năm";
        break;
      case 6:
        docHangTram = "Sáu";
        break;
      case 7:
        docHangTram = "Bảy";
        break;
      case 8:
        docHangTram = "Tám";
        break;
      case 9:
        docHangTram = "Chín";
        break;
    }
    switch (hangDonVi) {
      case 0:
        docHangDonVi = "Không";
        break;
      case 1:
        docHangDonVi = "Một";
        break;
      case 2:
        docHangDonVi = "Hai";
        break;
      case 3:
        docHangDonVi = "Ba";
        break;
      case 4:
        docHangDonVi = "Bốn";
        break;
      case 5:
        docHangDonVi = "Năm";
        break;
      case 6:
        docHangDonVi = "Sáu";
        break;
      case 7:
        docHangDonVi = "Bảy";
        break;
      case 8:
        docHangDonVi = "Tám";
        break;
      case 9:
        docHangDonVi = "Chín";
        break;
    }
    document.getElementById(
      "ketQuaBai3"
    ).innerHTML = `${docHangTram} trăm ${docHangDonVi}`;
  } else if (hangDonVi == 0) {
    alert("Số hàng đơn vị không được xác định");
    switch (hangTram) {
      case 0:
        docHangTram = "Không";
        break;
      case 1:
        docHangTram = "Một";
        break;
      case 2:
        docHangTram = "Hai";
        break;
      case 3:
        docHangTram = "Ba";
        break;
      case 4:
        docHangTram = "Bốn";
        break;
      case 5:
        docHangTram = "Năm";
        break;
      case 6:
        docHangTram = "Sáu";
        break;
      case 7:
        docHangTram = "Bảy";
        break;
      case 8:
        docHangTram = "Tám";
        break;
      case 9:
        docHangTram = "Chín";
        break;
    }
    switch (hangChuc) {
      case 0:
        docHangChuc = "Không";
        break;
      case 1:
        docHangChuc = "Một";
        break;
      case 2:
        docHangChuc = "Hai";
        break;
      case 3:
        docHangChuc = "Ba";
        break;
      case 4:
        docHangChuc = "Bốn";
        break;
      case 5:
        docHangChuc = "Năm";
        break;
      case 6:
        docHangChuc = "Sáu";
        break;
      case 7:
        docHangChuc = "Bảy";
        break;
      case 8:
        docHangChuc = "Tám";
        break;
      case 9:
        docHangChuc = "Chín";
        break;
    }
    document.getElementById(
      "ketQuaBai3"
    ).innerHTML = `${docHangTram} trăm ${docHangChuc} mươi `;
  } else {
    switch (hangTram) {
      case 0:
        docHangTram = "Không";
        break;
      case 1:
        docHangTram = "Một";
        break;
      case 2:
        docHangTram = "Hai";
        break;
      case 3:
        docHangTram = "Ba";
        break;
      case 4:
        docHangTram = "Bốn";
        break;
      case 5:
        docHangTram = "Năm";
        break;
      case 6:
        docHangTram = "Sáu";
        break;
      case 7:
        docHangTram = "Bảy";
        break;
      case 8:
        docHangTram = "Tám";
        break;
      case 9:
        docHangTram = "Chín";
        break;
    }
    switch (hangChuc) {
      case 0:
        docHangChuc = "Không";
        break;
      case 1:
        docHangChuc = "Một";
        break;
      case 2:
        docHangChuc = "Hai";
        break;
      case 3:
        docHangChuc = "Ba";
        break;
      case 4:
        docHangChuc = "Bốn";
        break;
      case 5:
        docHangChuc = "Năm";
        break;
      case 6:
        docHangChuc = "Sáu";
        break;
      case 7:
        docHangChuc = "Bảy";
        break;
      case 8:
        docHangChuc = "Tám";
        break;
      case 9:
        docHangChuc = "Chín";
        break;
    }
    switch (hangDonVi) {
      case 0:
        docHangDonVi = "Không";
        break;
      case 1:
        docHangDonVi = "Một";
        break;
      case 2:
        docHangDonVi = "Hai";
        break;
      case 3:
        docHangDonVi = "Ba";
        break;
      case 4:
        docHangDonVi = "Bốn";
        break;
      case 5:
        docHangDonVi = "Năm";
        break;
      case 6:
        docHangDonVi = "Sáu";
        break;
      case 7:
        docHangDonVi = "Bảy";
        break;
      case 8:
        docHangDonVi = "Tám";
        break;
      case 9:
        docHangDonVi = "Chín";
        break;
    }
    document.getElementById(
      "ketQuaBai3"
    ).innerHTML = `${docHangTram} trăm ${docHangChuc} mươi ${docHangDonVi}`;
  }
};

// Bài 4
document.getElementById("btnTim").onclick = function () {
  var sv1 = document.getElementById("tenSinhVien1").value;
  var X1 = document.getElementById("toaDoX1").value * 1;
  var Y1 = document.getElementById("toaDoY1").value * 1;

  var sv2 = document.getElementById("tenSinhVien2").value;
  var X2 = document.getElementById("toaDoX2").value * 1;
  var Y2 = document.getElementById("toaDoY2").value * 1;

  var sv3 = document.getElementById("tenSinhVien3").value;
  var X3 = document.getElementById("toaDoX3").value * 1;
  var Y3 = document.getElementById("toaDoY3").value * 1;

  var XTruongHoc = document.getElementById("toaDoXTruongHoc").value * 1;
  var YTruongHoc = document.getElementById("toaDoYTruongHoc").value * 1;

  var svXaTruongNhat = "";

  var khoangCach1 = Math.sqrt(
    (XTruongHoc - X1) * (XTruongHoc - X1) +
      (YTruongHoc - Y1) * (YTruongHoc - Y1)
  );
  var khoangCach2 = Math.sqrt(
    (XTruongHoc - X2) * (XTruongHoc - X2) +
      (YTruongHoc - Y2) * (YTruongHoc - Y2)
  );
  var khoangCach3 = Math.sqrt(
    (XTruongHoc - X3) * (XTruongHoc - X3) +
      (YTruongHoc - Y3) * (YTruongHoc - Y3)
  );

  if (khoangCach1 > khoangCach2 && khoangCach1 > khoangCach3) {
    svXaTruongNhat = sv1;
  }
  if (khoangCach2 > khoangCach1 && khoangCach2 > khoangCach3) {
    svXaTruongNhat = sv2;
  }
  if (khoangCach3 > khoangCach1 && khoangCach3 > khoangCach2) {
    svXaTruongNhat = sv3;
  }
  document.getElementById(
    "ketQuaBai4"
  ).innerHTML = `Sinh viên xa trường nhất : ${svXaTruongNhat} `;
};
